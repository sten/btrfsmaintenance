#!/bin/sh

man_date=$(date --utc -d "@$(dpkg-parsechangelog -STimestamp)" +"%Y-%m-%d")

cat << EOF > debian/tmp/btrfsmaintenance.md
% BTRFSMAINTENANCE 8 "$man_date" v$DEB_VERSION_UPSTREAM

NAME
====

**btrfsmaintenance** - automate btrfs maintenance tasks

NOTE
====

Refer to /usr/share/doc/btrfsmaintenance/README.Debian in addition to
this man page.

DESCRIPTION
===========
EOF

sed '1,8d' README.md >> debian/tmp/btrfsmaintenance.md
go-md2man -in=debian/tmp/btrfsmaintenance.md -out=debian/tmp/btrfsmaintenance.8
