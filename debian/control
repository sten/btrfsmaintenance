Source: btrfsmaintenance
Section: admin
Priority: optional
Maintainer: Nicholas D Steeves <sten@debian.org>
Build-Depends: debhelper-compat (= 13), dh-exec (>= 0.14)
             , dh-sequence-movetousr
             , go-md2man
Rules-Requires-Root: no
Standards-Version: 4.6.2
Homepage: https://github.com/kdave/btrfsmaintenance
Vcs-Git: https://salsa.debian.org/sten/btrfsmaintenance.git
Vcs-Browser: https://salsa.debian.org/sten/btrfsmaintenance

Package: btrfsmaintenance
Architecture: all
Depends: ${misc:Depends}
       , btrfs-progs
       , systemd | cron
Enhances: btrfs-progs
Description: automate btrfs maintenance tasks on mountpoints or directories
 This is a set of scripts for the btrfs filesystem that automates the
 following maintenance tasks: scrub, balance, trim, and defragment.
 .
 Tasks are enabled, disabled, scheduled, and customised from a single text
 file.  The default configuration assumes an installation profile where '/'
 is a btrfs filesystem.
 .
 The default values have been chosen as an even compromise between time to
 complete maintenance, improvement in filesystem performance, and
 minimisation of resources taken from other processes.  Please note that I/O
 priority scheduling requires the use of BFQ or CFQ, and not noop, deadline,
 anticipatory, nor the new mq-deadline which uses blk-mq.
